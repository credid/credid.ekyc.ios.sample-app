//
//  ViewController.swift
//  sampleSdkIntergrate
//
//  Created by Mac  on 05/09/19.
//  Copyright © 2019 Think. All rights reserved.
//

import UIKit
import EKYC

class ViewController: UIViewController {
    @IBOutlet weak var showResultSwitch: UISwitch!
    @IBOutlet weak var onlyTakePhotoSwitch: UISwitch!
    @IBOutlet weak var CountrySegmentController: UISegmentedControl!
    @IBOutlet weak var environmetSegmentController: UISegmentedControl!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func startKycAct(_ sender: Any) {
        let sdk = KoKuKYCSDK(client_id: "", secret: "", subscription: "", countryCode: getCountryCode(), callBackUrl: "", referenceID: "90285bf7-40e7-733b-9b0e-3a08f91791b2", enableResultScreen: showResultSwitch.isOn, allowImageUploadfromGallery: onlyTakePhotoSwitch.isOn, environment: getEnvironment(), verificationImageSingleSide: false, ignoreOCRScreen: false)
        
        sdk.delegate = self
        
        sdk.setTitleTextColor(color: UIColor.white)
        sdk.setBackgroundColor(color: UIColor(red: 97/255, green: 21/255, blue: 111/255, alpha: 1))
        sdk.setTitleBackgroundColor(color: UIColor(red: 97/255, green: 21/255, blue: 111/255, alpha: 1))
        
        sdk.startEkyc()
    }
    
    
    @IBAction func onShowResultChanged(_ sender: UISwitch) {
    }
    
    
    func getCountryCode() -> String{
        switch CountrySegmentController.selectedSegmentIndex{
        case 0:
            return "ID"
        case 1:
            return "SG"
        case 2:
            return "HK"
        default:
            return "ID"
        }
    }
    
    func getEnvironment() -> EkycEnvironment{

        switch environmetSegmentController.selectedSegmentIndex{
                   case 0:
                    return .DEVELOPMENT
                   case 1:
                    return .STAGING
                   case 2:
                    return .PRODUCTION
                   default:
                    return .DEVELOPMENT
        }
    }
}

extension ViewController: eKYCResultCallback{
    func onKYCError(kycResult: KYCError) {
        print("kycResult error \(kycResult.error) \n \(kycResult.message)")
    }
    
    func onKYCSuccess(kycResult: KYCResult) {
        print("kycResult \(kycResult.faceComparisonResponse!) \n \(kycResult.livenessResponse!) \n \(kycResult.ocrResponse!)")
    }
}
